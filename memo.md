
# jQuery
## jQuery去除String对象两侧的空格
`$.trim(String)`方法可以用来去除String对象的左右空格  
## jQuery文件引入顺序
在引入js文件时，注意引入顺序，基本上jQuery的本体文件要是第一个被引入的js文件  
## 表单的重制
*form*标签在IDEA中可以看到有`reset()`方法，但是jQuery对象调用它不会生效  
需要将jQuery对象转换为普通DOM对象后调用同名方法才行  
## 动态元素绑定事件
动态元素，比如列表项（使用代码拼接出来的）想要绑定事件的话，直接使用`JQUERY_OBJ.click()`这种方式是无法实现的  
需要使用`$(需绑定元素的有效的外层元素).on(事件句柄 ,需要绑定元素的jQuery对象 ,回调函数)`  
例如：
```
$("#activityBody").on("click", $("input[name=listItemCbox]"), function () {
	
})
```

# AJAX
## AJAX对象调用Servlet
注意*url*的前面没有'/'  
```
$.ajax({
	url : 'module_name/theServlet.do',
})
```

# Servlet
## url-pattern通配符注意项
filter映射时，如果要匹配一类后缀名，在web.xml中的url-pattern里，使用`*.后缀名`，不要使用`/*.后缀名`  
后者会导致服务器<sup>老师说的</sup>无法启动  
```
✅  /abc/myServlet.do
✅  /abc/*
✅  /*
✅  *.do
❌  /abc/*.do
❌  abc/*.do
```

## 重定向 和 转发
两者均建议使用绝对路径  
* 转发  
不需要加项目名，比如当前项目为**CRM**，转发地址写`/login.jsp`即转发至项目根目录的login.jsp页面  
* 重定向  
需要项目名，上例的需求，重定向使用`/CRM/login.jsp`实现  

# JSP
## basePath的使用
JSP文件默认代码  
用来指定网站基本路径，之后对于所有网站资源的引用使用网站的绝对路径，以`webapp`目录为根  
```
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>>">
```

## 9大内置对象
pageContext&emsp;page  
request&emsp;response  
session  
application&emsp;config  
out  
exception  
&emsp;&emsp;<sup>⚠️注意：</sup>EL表达式中的有部分相同的关键字，但是意义不同  
EL表达式中叫“隐含对象”  
比如：${pageContext.request}获取到的是请求对象(request)  
pageContext可以直接把信息存储到其它域对象中，其`setAttribute`方法可以接受3个参数，第3个参数指定该键值对的作用范围(PAGE_SCOPE/REQUEST_SCOPE/SESSION_SCOPE/APPLICATION_SCOPE)  

# JavaScript
## 应用EL表达式
在js中使用EL表达式，需要将表达式写为字符串形式，即`"EL_EXP"`形式  
## 动态代码中使用参数
与上方EL表达式一样，需要将参数写成字符串格式  
## 模态窗口的默认刷新页面行为
模态窗口触发回车事件后，默认会强制刷新当前页面  
这有可能导致刷新出不带任何数据的模板页  
所以如果模态窗口关联了回车键出发的功能，在方法的业务流程后面，令其return false, 则可以阻止该强制刷新动作  
